package io.gi0cann.android.timetracker;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class TaskListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        String sessionId = new Session(getApplicationContext()).getSessionId();
        if (sessionId == null) {
            Intent loginIntent = new Intent(TaskListActivity.this, LoginActivity.class);
            startActivity(loginIntent);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_list);
    }
}
