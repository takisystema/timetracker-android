package io.gi0cann.android.timetracker;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    public void loginToReg(View view) {
        Intent intent = new Intent(LoginActivity.this, RegistrationActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {

    }

    public void loginUser(View view) {
        TextView usernameView = (TextView) findViewById(R.id.login_username);
        TextView passwordView = (TextView) findViewById(R.id.login_password);
        final String username = usernameView.getText().toString();
        final String password = passwordView.getText().toString();

        class LoginTask extends AsyncTask<Void, Void, Integer> {

            @Override
            protected Integer doInBackground(Void... voids) {
                TimeTrackerApiClient timeapi = new TimeTrackerApiClient(getApplicationContext());
                Integer response = timeapi.login(username, password);
                return response;
            }

            @Override
            protected void onPostExecute(Integer response) {
                Log.d("TIMAPI.LOGIN", response.toString());
                if (response == 200) {
                    Toast.makeText(LoginActivity.this, "Login Successful :)", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(LoginActivity.this, UserListActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(LoginActivity.this, "Login Failed :(", Toast.LENGTH_LONG).show();
                }
            }
        }

        new LoginTask().execute();
    }
}
