package io.gi0cann.android.timetracker;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class UserListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);
        getUser();
    }

    public void getUser() {

        class getUserTask extends AsyncTask<Void, Void, String> {

            @Override
            protected String doInBackground(Void... voids) {
                TimeTrackerApiClient timeapi = new TimeTrackerApiClient(getApplicationContext());
                String id = getApplicationContext().getSharedPreferences("session", MODE_PRIVATE).getString("userid", null);
                String response = timeapi.getUser(id);
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                ListView listView = (ListView) findViewById(R.id.user_list_view);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getBaseContext(), R.layout.users_text_view, R.id.user_text_view);
                try {
                    JSONArray responseJSON = new JSONArray(response);
                    String username = responseJSON.getJSONObject(0).getString("username");
                    String email = responseJSON.getJSONObject(0).getString("email");
                    String objid = responseJSON.getJSONObject(0).getString("_id");
                    //String isAdmin = responseJSON.getJSONObject(0).getString("isAdmin");
                    String active = responseJSON.getJSONObject(0).getString("active");
                    adapter.add("User: " + username);
                    adapter.add("Email: " + email);
                    adapter.add("id: " + objid);
                    //adapter.add("isAdmin: " + isAdmin);
                    adapter.add("active: " + active);
                    listView.setAdapter(adapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        new getUserTask().execute();
    }
}
