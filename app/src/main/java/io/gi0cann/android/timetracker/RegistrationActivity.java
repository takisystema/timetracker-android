package io.gi0cann.android.timetracker;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.net.URL;

public class RegistrationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
    }

    public void registerUser(View view) {
        TextView usernameView = (TextView) findViewById(R.id.registration_username);
        TextView passwordView = (TextView) findViewById(R.id.registration_password);
        TextView passwordConfirmView = (TextView) findViewById(R.id.registration_confirm);
        TextView emailView = (TextView) findViewById(R.id.registration_email);
        final String username = usernameView.getText().toString();
        final String password = passwordView.getText().toString();
        String password_confirm = passwordConfirmView.getText().toString();
        final String email = emailView.getText().toString();

        class RegisterTask extends AsyncTask<URL, Void, Integer> {

            @Override
            protected Integer doInBackground(URL... urls) {
                TimeTrackerApiClient timeapi = new TimeTrackerApiClient(getApplicationContext());
                Integer response = timeapi.register(username, email, password);
                return response;
            }

            @Override
            protected void onPostExecute(Integer response) {
                Log.d("TIMAPI.REGISTER", response.toString());
                if (response == 201) {
                    Toast.makeText(RegistrationActivity.this, "Registration Successful :)", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(RegistrationActivity.this, LoginActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(RegistrationActivity.this, "Registration Failed :(", Toast.LENGTH_LONG).show();
                }
            }
        }

        new RegisterTask().execute();

    }
}
