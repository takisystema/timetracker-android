package io.gi0cann.android.timetracker;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;

public class TimeTrackerApiClient {

    private String api;
    private URL url;
    private Context AppContext;

    public TimeTrackerApiClient(Context appContext) {
       //api = "http://timetracker.gi0cann.io:27995/api";
        api = "https://desolate-brook-41820.herokuapp.com/api";
        AppContext = appContext;
    }

    private String read(HttpURLConnection conn) {
        try {
            StringBuffer response = new StringBuffer();
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            //http.setFixedLengthStreamingMode(length);
            return response.toString();
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return "Fail";
        } catch (IOException e) {
            e.printStackTrace();
            return "Fail IOException";
        }
    }

    private Object write(HttpURLConnection conn, byte[] bodyBytes) {
        try {
            OutputStream out = new BufferedOutputStream(conn.getOutputStream());
            //http.setFixedLengthStreamingMode(length);
            out.write(bodyBytes);
            out.close();
            return conn.getResponseCode();
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return -1;
        } catch (IOException e) {
            e.printStackTrace();
            return -1;
        }
    }

    public HttpURLConnection get(String endpoint) {
        try {
            url = new URL(api.concat(endpoint));
            URLConnection urlCon = url.openConnection();
            HttpURLConnection http = (HttpURLConnection) urlCon;
            http.setRequestMethod("GET");
            http.setRequestProperty("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoiNWNjZGI3ODJiZDI4YTE1OGM5MDMyOGJhIiwidXNlcm5hbWUiOiJkZWJ1c3N5IiwiZW1haWwiOiJkb25zYXNvbkBob3RtYWlsLmNvbSJ9LCJpYXQiOjE1NTY5ODYxNTB9.x_RW-ttACgcizsF8Q32wajJlpth8g7jW3D3CYqY-Q58");
            return http;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
    public String put(String id, String endpoint, String body) {
        return null;
    }

    public int post(String endpoint, String body) {
        try {
            byte[] bodyBytes = body.getBytes("UTF-8");
            int length = bodyBytes.length;
            url = new URL(api.concat(endpoint));
            URLConnection urlCon = url.openConnection();
            HttpURLConnection http = (HttpURLConnection) urlCon;
            http.setRequestMethod("POST");
            http.setDoOutput(true);
            http.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            http.setRequestProperty("Content-Length", String.valueOf(length));
            try {
                OutputStream out = new BufferedOutputStream(http.getOutputStream());
                //http.setFixedLengthStreamingMode(length);
                out.write(bodyBytes);
                out.close();
                return http.getResponseCode();
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return -1;
            } finally {
                http.disconnect();
            }
        } catch (IOException e) {
            e.printStackTrace();
            return -1;
        }
    }

    public HttpURLConnection post(String endpoint, JSONObject body) {
        try {
            Log.d(body.toString(), "TIMEAPI.POST");
            byte[] bodyBytes = body.toString().getBytes("UTF-8");
            int length = bodyBytes.length;
            url = new URL(api.concat(endpoint));
            URLConnection urlCon = url.openConnection();
            HttpURLConnection http = (HttpURLConnection) urlCon;
            http.setRequestMethod("POST");
            http.setDoOutput(true);
            http.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            http.setRequestProperty("Content-Length", String.valueOf(length));
            write(http, bodyBytes);
            return http;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String delete(String id, String endpoint, String body) {
        return null;
    }

    public int register(String username, String email, String password) {
        Map<String, String> bodyMap = new HashMap<String, String>();
        bodyMap.put("username", username);
        bodyMap.put("email", email);
        bodyMap.put("password", password);
        JSONObject body = new JSONObject(bodyMap);
        HttpURLConnection conn = post("/user/register", body);
        try {
            return conn.getResponseCode();
        } catch (IOException e) {
            e.printStackTrace();
            return -1;
        }
    }

    public int login(String email, String password) {
        Map<String, String> bodyMap = new HashMap<String, String>();
        bodyMap.put("email", email);
        bodyMap.put("password", password);
        JSONObject body = new JSONObject(bodyMap);
        Log.d("TIMEAPI.LOGIN.BODY", body.toString());
        HttpURLConnection conn = post("/user/login", body);
        Map<String, String> responseMap = new HashMap<String, String>();
        try {
            int status_code = conn.getResponseCode();
            if (status_code == 200) {
                String response = read(conn);
                Log.d("LOGINRESPONSE", "response: " + response);
                try {
                    JSONObject responseJSON = new JSONObject(response);
                    String sessionToken = responseJSON.getString("sessionToken");
                    String userid = responseJSON.getString("userid");
                    Log.d("TIMEAPI.LOGIN.USERID", "userid" + userid);
                    SharedPreferences.Editor editor = AppContext.getSharedPreferences("session", Context.MODE_PRIVATE).edit();
                    editor.putString("token", sessionToken);
                    editor.putString("userid", userid);
                    editor.apply();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return status_code;
        } catch (IOException e) {
            e.printStackTrace();
            return -1;
        }
    }


    public String getUser(String uid) {
        Log.d("TIMEAPI.GETUSER.USERID", "userid" + uid);
        HttpURLConnection conn = get("/user/" + uid);
        try {
            int respcode = conn.getResponseCode();
            if (respcode == 200) {
                String response = read(conn);
                Log.d("TIMEAPI.GETUSERS.RESP", response);
                return response;
            } else {
                Log.d("TIMEAPI.GETUSERS.RESP", "Request failed" + String.valueOf(respcode));
                return "-1";
            }
        } catch (IOException e) {
            e.printStackTrace();
            return "-1";
        }
    }


}
