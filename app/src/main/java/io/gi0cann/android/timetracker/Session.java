package io.gi0cann.android.timetracker;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Session {

    private SharedPreferences prefs;

    public Session(Context cntx) {
        prefs = PreferenceManager.getDefaultSharedPreferences(cntx);
    }

    public void setSessionId(String sessionId) {
        prefs.edit().putString("sessionId", sessionId).apply();
    }

    public String getSessionId() {
        String sessionId = prefs.getString("sessionId", null);
        return sessionId;
    }

    public void deleteSessionId() {
        prefs.edit().remove("sessionId");
    }
}
